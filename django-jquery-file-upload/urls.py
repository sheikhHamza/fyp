
from django.http import HttpResponseRedirect
from django.conf import settings
from django.conf.urls import  include, url
from django.urls import include, path
from fileupload import views as myview

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
url(r'^test', myview.test),
url(r'^test1', myview.test1),
    path('upload/new', lambda x: HttpResponseRedirect('/upload/new/')),
    path('upload/', include('fileupload.urls')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
