# encoding: utf-8
import json
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.views.generic import CreateView, DeleteView, ListView
from django.template.response import TemplateResponse

from .models import Picture
from .response import JSONResponse, response_mimetype
from .serialize import serialize
import cv2
import glob
import numpy as np
from crop_header  import main
from Check_for_Forgery import forgery_check
from keras import backend as K
#from sklearn.preprocessing import StandardScaler
#from sklearn.decomposition import PCA
#from sklearn.ensemble import IsolationForest
#from sklearn import svm
import glob
from django.http import HttpResponseRedirect




def test1(request):

  img = cv2.imread("//django-jquery-file-upload/media/Pictures/test.jpg")

  if(forgery_check(img)):
    return HttpResponse("Original")
  else:
      return HttpResponse("Fake")



def extract_resnet(X):  
    '''
    [n,h,w,c] = np.array(X).shape

    resnet_model = ResNet50(input_shape=(h, w, c), weights='imagenet',pooling='max' ,include_top=False)  # Since top layer is the fc layer used for predictions
    features_array = resnet_model.predict(np.array(X),verbose=1)
    print(features_array.shape)

    return features_array
    '''
    return HttpResponse ('ok')


def test(request):
    '''
    cv_img = []
    for img in glob.glob("/home/abdul/Desktop/Fyp2/django-jquery-file-upload/media/Pictures/*.jpg"):
        n= cv2.imread(img)
        #cv2.imshow('Test Image', n)
        #key = cv2.waitKey(0)
        #if key == 27:
        #    break

        cv_img.append(n)


    


    X_train = np.load('TraningFeaturesMatrix.npy')
    #X_test =np.load('TestFeaturesMatrix.npy')
    #X_test.append(iamge)
    
    X_test = extract_resnet(cv_img)


    message=True
    return render_to_response('picture_form.html', {'courts':message })

    #X_test = extract_resnet(X_test_images)
    # Apply standard scaler to output from resnet50
    ss = StandardScaler()
    ss.fit(X_train)
    X_train = ss.transform(X_train)
    X_test = ss.transform(X_test)

    # Take PCA to reduce feature space dimensionality
    pca = PCA(n_components=512, whiten=True)
    pca = pca.fit(X_train)
    print('Explained variance percentage = %0.2f' % sum(pca.explained_variance_ratio_))
    X_train = pca.transform(X_train)
    X_test = pca.transform(X_test)

    # Train classifier and obtain predictions for OC-SVM
    #oc_svm_clf = svm.OneClassSVM(gamma=0.001, kernel='rbf', nu=0.08)  # Obtained using grid search
    if_clf = IsolationForest(contamination=0.08, max_features=1.0, max_samples=1.0, n_estimators=40)  # Obtained using grid search

    #oc_svm_clf.fit(X_train)
    if_clf.fit(X_train)

    #oc_svm_preds = oc_svm_clf.predict(X_test)
    if_preds = if_clf.predict(X_test)

    #print(oc_svm_preds)
    #print(if_preds)

    #return HttpResponse(if_preds)
    if(if_preds[0] ==1):
        message=True
    if(if_preds[0] ==-1):
        message=False
    
    keras.backend.clear_session() 
    '''
    img = cv2.imread("/home/sheikh/Desktop/Untitled Folder/Fyp2/django-jquery-file-upload/media/Pictures/test.jpg")
    
    #img = main(img)


    if(forgery_check(img)):
        message = True
    else:
        message = False

    K.clear_session()
    return render_to_response('picture_form.html', {'courts':message })
    #return TemplateResponse(request, '/fileupload/picture_form.html', {'entries': message})
    
    


class PictureCreateView(CreateView):
    model = Picture
    fields = "__all__"

    def form_valid(self, form):
        self.object = form.save()
        files = [serialize(self.object)]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

    def form_invalid(self, form):
        data = json.dumps(form.errors)
        return HttpResponse(content=data, status=400, content_type='application/json')

class BasicVersionCreateView(PictureCreateView):
    template_name_suffix = '_basic_form'


class BasicPlusVersionCreateView(PictureCreateView):
    template_name_suffix = '_basicplus_form'


class AngularVersionCreateView(PictureCreateView):
    template_name_suffix = '_angular_form'


class jQueryVersionCreateView(PictureCreateView):
    template_name_suffix = '_jquery_form'


class PictureDeleteView(DeleteView):
    model = Picture

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        response = JSONResponse(True, mimetype=response_mimetype(request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class PictureListView(ListView):
    model = Picture

    def render_to_response(self, context, **response_kwargs):
        files = [ serialize(p) for p in self.get_queryset() ]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response